/**
 *
 * @file Config.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.KeyPair;
import java.util.Properties;

import sftp.lib.Console;
import sftp.lib.Utility;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.io.FileManage;

public class Config {
	private static final String DEFAULT_FILENAME = "Server.config.properties";
	private static final String DEFAULT_DBCONNECT = "jdbc:mysql://localhost:3306/sftp?useUnicode=true&characterEncoding=utf-8";
	private static final String DEFAULT_DBUSER = "root";
	private static final String DEFAULT_DBPASS = "";
	private static final int DEFAULT_PORT = 8000;
	private static final int DEFAULT_MAXCONNECT = 255;
	private static final String DEFAULT_SHAREDKEY = "iQx+6N5uVzIA0xhq73sNzH1jbPZRWMvoopgjEBrgedU=";
	private static final String DEFAULT_SERVDIR = "serv";
	private static final int DEFAULT_MaxPackageSize = 100000;
	protected String dburl;
	protected String dbuser;
	protected String dbpass;
	protected int port;
	protected int maxConnect;
	//server's public & private key
	protected String key;
    //use for default authentication
    private String sharedKey;
    private String servdir;
    private int maxPkgSize;
    
	public Config(){
		
	}
	
	public Config(Config c){
		this.dburl = c.dburl;
		this.dbuser = c.dbuser;
		this.dbpass = c.dbpass;
		this.port = c.port;
		this.maxConnect = c.maxConnect;
		this.sharedKey = c.sharedKey;
		this.key = c.key;
		this.servdir = c.servdir;
		this.maxPkgSize = c.maxPkgSize;
	}
	
	/**
	 * @return the dburl
	 */
	public String getDburl() {
		return dburl;
	}



	/**
	 * @param dburl the dburl to set
	 */
	public void setDburl(String dburl) {
		this.dburl = dburl;
	}



	/**
	 * @return the dbuser
	 */
	public String getDbuser() {
		return dbuser;
	}



	/**
	 * @param dbuser the dbuser to set
	 */
	public void setDbuser(String dbuser) {
		this.dbuser = dbuser;
	}



	/**
	 * @return the dbpass
	 */
	public String getDbpass() {
		return dbpass;
	}



	/**
	 * @param dbpass the dbpass to set
	 */
	public void setDbpass(String dbpass) {
		this.dbpass = dbpass;
	}



	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}



	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	
	
	/**
	 * @return the maxConnect
	 */
	public int getMaxConnect() {
		return maxConnect;
	}



	/**
	 * @param maxConnect the maxConnect to set
	 */
	public void setMaxConnect(int maxConnect) {
		this.maxConnect = maxConnect;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the sharedKey
	 */
	public String getSharedKey() {
		return sharedKey;
	}

	/**
	 * @param sharedKey the sharedKey to set
	 */
	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}

	/**
	 * @return the servdir
	 */
	public String getServdir() {
		return servdir;
	}

	/**
	 * @param servdir the servdir to set
	 */
	public void setServdir(String servdir) {
		this.servdir = servdir;
	}

	public int getMaxPkgSize() {
		return maxPkgSize;
	}

	public void setMaxPkgSize(int maxPkgSize) {
		this.maxPkgSize = maxPkgSize;
	}

	public static boolean SaveConfig(Config c){
		if(c == null){
			return false;
		}
		boolean flag = false;
		Properties prop = new Properties();
		OutputStream output = null;
	 
		try {
	 
			output = new FileOutputStream(DEFAULT_FILENAME);
	 
			// set the properties value
			prop.setProperty("dburl", c.getDburl());
			prop.setProperty("dbuser", c.getDbuser());
			prop.setProperty("dbpass", c.getDbpass());
			prop.setProperty("port", Integer.toString(c.getPort()));
			prop.setProperty("maxConnect", Integer.toString(c.getMaxConnect()));
			prop.setProperty("key", c.getKey());
			prop.setProperty("sharedkey", c.getSharedKey());
			prop.setProperty("servdir", c.getServdir());
			prop.setProperty("maxPkgSize", Long.toString(c.getMaxPkgSize()));
	 
			// save properties to project root folder
			prop.store(output, null);
			
			flag = true;
	 
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
		}
		return flag;
	}


	public static Config LoadConfig(){
		Config c = new Config();
		Properties prop = new Properties();
		InputStream input = null;
	 
		try {
	 
			input = new FileInputStream(DEFAULT_FILENAME);
	 
			// load a properties file
			prop.load(input);
	 
			// get the property value and print it out
			c.setDburl (prop.getProperty("dburl", DEFAULT_DBCONNECT));
			c.setDbuser(prop.getProperty("dbuser", DEFAULT_DBUSER));
			c.setDbpass(prop.getProperty("dbpass", DEFAULT_DBPASS));
			c.setPort(Utility.ParseInt(prop.getProperty("port"), DEFAULT_PORT));
			c.setMaxConnect(Utility.ParseInt(prop.getProperty("maxConnect"), DEFAULT_MAXCONNECT));
			c.setKey(prop.getProperty("key", ""));
	        if(c.getKey().equals("")){
	        	KeyPair pair = KDC.generateRSAKey();
	        	String newkey = Utility.printBase64Binary(ObjectHandler.ToBytes(pair));
	        	c.setKey(newkey);
	        	SaveConfig(c);
	        }
	        c.setSharedKey(prop.getProperty("sharedkey", DEFAULT_SHAREDKEY));
	        String dir = prop.getProperty("servdir", DEFAULT_SERVDIR);
	        if(! FileManage.checkDir(dir)){
	        	if(! FileManage.createDir(dir)){
	        		Console.println("Error: create directory failed.");
	        	}
	        }
	        c.setServdir(dir);
	        c.setMaxPkgSize(Utility.ParseInt(prop.getProperty("maxPkgSize"), DEFAULT_MaxPackageSize));
		} catch (IOException ex) {
			Console.println("Failed to load config file. Default value will be used.");
			c.setDburl(DEFAULT_DBCONNECT);
			c.setDbuser(DEFAULT_DBUSER);
			c.setDbpass(DEFAULT_DBPASS);
			c.setPort(DEFAULT_PORT);
			c.setMaxConnect(DEFAULT_MAXCONNECT);
        	KeyPair pair = KDC.generateRSAKey();
        	c.setKey(Utility.printBase64Binary(ObjectHandler.ToBytes(pair)));
        	c.setSharedKey(DEFAULT_SHAREDKEY);
        	c.setServdir(DEFAULT_SERVDIR);
        	c.setMaxPkgSize(DEFAULT_MaxPackageSize);
			SaveConfig(c);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return c;
	}
}
