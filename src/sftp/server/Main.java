/**
 *
 * @file Main.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.security.KeyPair;
import java.util.Collection;
import java.util.Scanner;

import javax.persistence.PersistenceException;

import sftp.lib.Console;
import sftp.lib.Utility;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.crypto.MD5;
import sftp.lib.crypto.SHA256;
import sftp.lib.io.FileManage;
import sftp.server.entity.Disk;
import sftp.server.entity.User;

public class Main {
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Config conf = Config.LoadConfig();
		Serializable s = ObjectHandler.FromBytes(Utility.parseBase64Binary(conf.getKey()));
		Server server = null;
		if(s instanceof KeyPair){
			KeyPair key = (KeyPair)s;
			KDC.setServerPrivateKey(key.getPrivate());
			KDC.setServerPublicKey(key.getPublic());
		}else{
			Console.println("Error: can not analysis server key.");
			System.exit(-1);
		}
		try{
			System.out.println("Welcome to SFTP Server " + sftp.lib.Version.getVersion());
			System.out.println("If need help, please type \"help\".");
			Database db = new Database(conf);
			ClientManageCenter.setDb(db);
			db.createTransactionalEntityManager();db.closeTransactionalEntityManager();
			ObjectHandler.setSharedkey(conf.getSharedKey());
			server = new Server(db, conf);
			server.start();
			Scanner input = new Scanner(System.in);
			while(true){
				String cmd = input.next();
				if(cmd.equals("quit") || cmd.equals("exit")){
					server.close();
					break;
				}else if(cmd.equals("close")){
					server.close();
				}else if(cmd.equals("start")){
					server = new Server(db, conf);
					server.start();
				}else if(cmd.equals("add")){
					String user = input.next();
					String pass = input.next();
					int size = input.nextInt();
					CreateNewUser(user, pass, size, conf);
				}else if(cmd.equals("del")){
					String user = input.next();
					User u = ClientManageCenter.getUserByUserName(user);
					if(u == null){
						Console.println("user not found.");	
					}else{
						ClientManageCenter.DeleteUser(u);
						Console.println("user del success.");	
					}
				}else if(cmd.equals("change")){
					String type = input.next();
					if(type.equals("serverkey")){
						KeyPair pair = KDC.generateRSAKey();
						conf.setKey(Utility.printBase64Binary(ObjectHandler.ToBytes(pair)));
						Config.SaveConfig(conf);
						KDC.setServerPrivateKey(pair.getPrivate());
						KDC.setServerPublicKey(pair.getPublic());
				        SHA256 sha = new SHA256();
				        byte[] bytes = sha.Encrypt(pair.getPublic());
				        String servHash = sftp.lib.Utility.printBase64Binary(bytes);
				        System.out.println("Please update client's server hash key.");
				        System.out.println("key is " + servHash);
				        try{
				        	BufferedWriter writer = new BufferedWriter(new FileWriter(new File("servhash.txt")));
				        	writer.write("key is " + servHash);
				        	writer.close();
				        	System.out.println("Save the result to servhash.txt");
				        }catch (Exception e){
				        }
					}else if(type.equals("sharedkey")){
						String sharedkey = sftp.lib.Utility.printBase64Binary(KDC.generateAESKey());
						conf.setSharedKey(sharedkey);
						Config.SaveConfig(conf);
						ObjectHandler.setSharedkey(sharedkey);
						System.out.println("Please update client's shared key.");
				        System.out.println("key is " + sharedkey);
				        try{
				        	BufferedWriter writer = new BufferedWriter(new FileWriter(new File("sharedkey.txt")));
				        	writer.write("key is " + sharedkey);
				        	writer.close();
				        	System.out.println("Save the result to sharedkey.txt");
				        }catch (Exception e){
				        }
					}else{
						Console.println("Command Type wrong, Please input again.");
						help();
					}
				}else if(cmd.equals("help")){
					help();
				}else{
					Console.println("Wrong Command.");
					help();
				}
			}
			input.close();
		}
		catch(PersistenceException e){
			Console.println("Error: failed to connect database, please check again.");
			System.exit(-1);
		}
		catch(Exception e){
			Console.println(e.getMessage());
			System.exit(-1);			
		}
	}
	
	private static void CreateNewUser(String user, String pass, int size, Config conf){
		User u = ClientManageCenter.getUserByUserName(user);
		if(u != null){
			Console.println("user already exist.");	
			return;
		}
		MD5 md5 = new MD5();
		pass = md5.EncryptToString(pass);
		User newUser = new User(user, pass);
		Disk disk = new Disk();
		disk.setTotalSpace(Long.valueOf(size)*1024*1024);
		disk.setUser(newUser);
		disk.setDirectory(newUser.getUsername());
		newUser.setDiskId(disk);
		ClientManageCenter.addUser(newUser);
		
		String servDir = conf.getServdir();
		String userDir = servDir + File.separator + user;
		FileManage.createDir(userDir);
		String musicDir = userDir + File.separator + "Music";
		String videoDir = userDir + File.separator + "Video";
		String DocDir = userDir + File.separator + "Document";
		String PicDir = userDir + File.separator + "Picture";
		FileManage.createDir(musicDir);
		FileManage.createDir(videoDir);
		FileManage.createDir(DocDir);
		FileManage.createDir(PicDir);
		Console.println("add user success.");		
	}
	
	private static void help(){
		Console.println("Usage: ");
		Console.println("\tAdd user: add <username> <password> <disk capacity>");
		Console.println("\tDelete user: del <username>");
		Console.println("\tStart Server: start");
		Console.println("\tClose Server: close");
		Console.println("\tExit Application: exit/quit");
		Console.println("\tChanged the server's key: change serverkey");
		Console.println("\tChanged the sharedkey: change sharedkey");
	}

}
