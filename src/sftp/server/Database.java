/**
 *
 * @file database.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Database {
	private static EntityManagerFactory emf;
    private static EntityManager em;
    
	public Database(Config conf){
		emf = Persistence.createEntityManagerFactory("SFTP_Server", getProperties(conf));
	}
	
    /**
	 * @return the em
	 */
	public EntityManager getEm() {
		return em;
	}


	public synchronized void createTransactionalEntityManager() {

        // Create a new EntityManager
        em = emf.createEntityManager();

        // Begin transaction
        em.getTransaction().begin();
    }
    
    public synchronized void closeTransactionalEntityManager() {

        // Commit the transaction
        em.getTransaction().commit();

        // Close this EntityManager
        em.close();
    }
    
    public synchronized void createEntityManager() {

        // Create a new EntityManager
        em = emf.createEntityManager();
    }

    public synchronized void closeEntityManager() {

        // Close this EntityManager
        em.close();
    }
	
	public Map<String, String> getProperties(Config c){
		Map<String, String> properties = new HashMap<String, String>();
		/*
		properties.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		properties.put("hibernate.connection.url", c.getDburl());
		properties.put("hibernate.connection.username", c.getDbuser());
		properties.put("hibernate.connection.password", c.getDbpass());
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show-sql", "true");
		*/
		properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		properties.put("javax.persistence.jdbc.url", c.getDburl());
		properties.put("javax.persistence.jdbc.user", c.getDbuser());
		properties.put("javax.persistence.jdbc.password", c.getDbpass());
		properties.put("eclipselink.jdbc.exclusive-connection.mode", "Always");
		properties.put("eclipselink.jdbc.exclusive-connection.is-lazy", "false");
		return properties;
	}
}
