package sftp.server;

import java.net.InetAddress;
import java.security.PublicKey;

import sftp.server.entity.User;

public class Client {
	protected InetAddress ipaddr;
	protected PublicKey publickey;
	protected byte[] sessionKey;
	protected User user;
	protected boolean isAuthorized;
	protected int threadCount;
	
	public Client(InetAddress ipaddr){
		this.ipaddr = ipaddr;
		this.isAuthorized = false;
		this.threadCount = 1;
	}
}
