/**
 *
 * @file ClientManageCenter.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.sql.Time;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import sftp.lib.Console;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.Entity.UserLogin;
import sftp.server.entity.*;

public class ClientManageCenter {
	private static HashMap<String, Client> clientList = new HashMap<String, Client>();
	private static Database db;

	public static void addClient(Client client) throws Exception {
		String newkey = String.format("%s:%s", client.ipaddr.toString(), client.user.getUsername());
		if(clientList.get(newkey) != null){
			throw new Exception("Duplicate login.");
		}
		clientList.put(newkey, client);
	}
	
	public static Client getClientByKey(String key){
		return clientList.get(key);
	}

	public static Client removeClient(Client client) {
		if(!client.isAuthorized){
			return clientList.remove(client.ipaddr.toString());
		}
		else{
			String key = String.format("%s:%s", client.ipaddr.toString(), client.user.getUsername());
			if (client.threadCount == 1) {
				return clientList.remove(key);
			} else {
				Client c = clientList.get(key);
				synchronized(c){
					c.threadCount = c.threadCount - 1;
				}
				return c;
			}
		}
	}

	/**
	 * @return the clientList
	 */
	public static HashMap<String, Client> getClientList() {
		return clientList;
	}

	public static void addUser(User user) {
		if (db == null) {
			Console.println("database missing.");
			return;
		}
		db.createTransactionalEntityManager();
		db.getEm().persist(user);
		db.closeTransactionalEntityManager();
	}

	public static User checkUser(UserLogin userlogin, Client client) {
		if (db == null) {
			Console.println("database missing.");
			return null;
		}
		db.createEntityManager();
		Query q = db
				.getEm()
				.createQuery(
						"select c from User c where c.username = :username and c.password = :password");
		q.setParameter("username", userlogin.getUsername());
		q.setParameter("password", userlogin.getPassword());
		User user = null;
		try {
			user = (User) q.getSingleResult();
			if (user != null) {
				user.setLastVisit(new Time(System.currentTimeMillis()));
				user.setIp(client.ipaddr.toString());
			}
		} catch (NoResultException e) {
		}
		db.closeEntityManager();
		return user;
	}

	public static User getUserByUserName(String username) {
		if (db == null) {
			Console.println("database missing.");
			return null;
		}
		db.createEntityManager();
		Query q = db.getEm().createQuery(
				"select c from User c where c.username = :username");
		q.setParameter("username", username);
		User user = null;
		try {
			user = (User) q.getSingleResult();
		} catch (NoResultException e) {
		}
		db.closeEntityManager();
		return user;
	}

	public synchronized static User DeleteFiles(User user, DirectoryInfo info) {
		if (db == null) {
			Console.println("database missing.");
			return user;
		}

		db.createTransactionalEntityManager();
		User u = db.getEm().merge(user);
		String dir = info.getDirectory();
		if (!dir.endsWith("/")) {
			dir += "/";
		}
		long totalDiff = 0;
		for (FileInfo f : info.getFileLists()) {
			Query q = db
					.getEm()
					.createQuery(
							"select c from File c where c.fileName = :filename and c.Disk = :diskid and c.directory = :directory");
			q.setParameter("filename", f.getFileName());
			q.setParameter("diskid", u.getDiskId());
			q.setParameter("directory", dir);
			sftp.server.entity.File rf = null;
			try {
				if (q.getResultList().size() == 1) {
					rf = (sftp.server.entity.File) q.getSingleResult();
					totalDiff += rf.getSize();
					db.getEm().remove(rf);
				} else {
					// remove duplicate data
					List<sftp.server.entity.File> lists = q.getResultList();
					for (int i = 0; i < lists.size(); i++) {
						db.getEm().remove(lists.get(i));
						totalDiff += lists.get(i).getSize();
					}
				}
			} catch (NoResultException e) {
				e.printStackTrace();
			}
		}
		long newusage = u.getDiskId().getUseSpace() - totalDiff;
		if (newusage < 0)
			newusage = 0;
		u.getDiskId().setUseSpace(newusage);

		db.closeTransactionalEntityManager();
		return u;
	}

	public synchronized static User UpdateUserDisk(User user, String directory,
			String fileName, long fileSize) throws Exception {
		if (db == null) {
			Console.println("database missing.");
			return user;
		}
		boolean isExist = false;

		db.createTransactionalEntityManager();
		User u = db.getEm().merge(user);

		long newusage = 0;

		Query q = db
				.getEm()
				.createQuery(
						"select c from File c where c.fileName = :filename and c.Disk = :diskid and c.directory = :directory");
		q.setParameter("filename", fileName);
		q.setParameter("diskid", u.getDiskId());
		q.setParameter("directory", directory);
		sftp.server.entity.File sf = null;
		try {
			sf = (sftp.server.entity.File) q.getSingleResult();
		} catch (NoResultException e) {
		}
		if (sf != null) {
			sf.setCreateTime(new Time(System.currentTimeMillis()));
			long originSize = sf.getSize();
			sf.setSize(fileSize);
			newusage = u.getDiskId().getUseSpace() + fileSize - originSize;
			isExist = true;
		} else {
			isExist = false;
		}

		if (!isExist) {
			sftp.server.entity.File f = new sftp.server.entity.File(directory,
					fileName, fileSize, new Time(System.currentTimeMillis()));
			f.setDisk(u.getDiskId());
			u.getDiskId().getFiles().add(f);
			newusage = u.getDiskId().getUseSpace() + fileSize;
		}

		if (newusage > u.getDiskId().getTotalSpace()) {
			throw new Exception("Error: User disk is full");
		}
		u.getDiskId().setUseSpace(newusage);

		db.closeTransactionalEntityManager();
		return u;
	}

	public synchronized static void UpdateUserInfo(User user) {
		if (db == null) {
			Console.println("database missing.");
			return;
		}
		db.createTransactionalEntityManager();
		User updateUser = db.getEm().find(User.class, user.getId());
		updateUser.clone(user);
		db.closeTransactionalEntityManager();
	}

	public synchronized static void DeleteUser(User user) {
		db.createTransactionalEntityManager();
		// Merge the user to the new persistence context
		User u = db.getEm().merge(user);
		Disk d = u.getDiskId();
		db.getEm().remove(d);
		db.getEm().remove(u);
		db.closeTransactionalEntityManager();
	}

	/**
	 * @param db
	 *            the db to set
	 */
	public static void setDb(Database db) {
		ClientManageCenter.db = db;
	}

}
