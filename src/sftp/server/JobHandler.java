/**
 *
 * @file JobHandler.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.net.*;
import java.nio.file.NoSuchFileException;
import java.security.PublicKey;
import java.io.*;

import sftp.lib.crypto.AES;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.crypto.RSA;
import sftp.lib.crypto.SHA256;
import sftp.lib.io.FileManage;
import sftp.lib.socket.*;
import sftp.lib.socket.Entity.*;
import sftp.lib.Console;
import sftp.lib.Utility;
import sftp.server.Client;
import sftp.server.entity.User;

public class JobHandler extends Thread {
	// 1M bytes per package
	private int MAXTransfer;
	private Socket Sock;
	private byte[] recData;
	private byte[] recTmpData;
	private String baseDirectory;
	private FileInputStream fis;
	private File file;
	private long preIndex;
	private byte[] FileRecData;
	private byte[] Data;
	private Client client;

	public JobHandler(Socket s, String baseDir, int max) {
		this.Sock = s;
		this.baseDirectory = baseDir;
		this.MAXTransfer = max;
		this.Data = new byte[MAXTransfer];
	}

	@Override
	public void run() {
		InetAddress addr = Sock.getInetAddress();
		this.client = new Client(addr);
		try {
			OutputStream writer = Sock.getOutputStream();
			InputStream reader = Sock.getInputStream();
			SocketHelper helper = new SocketHelper(reader);
			boolean Isfinish = false;

			while (!Isfinish) {
				recData = helper.readSocket();
				if (recData.length == 0)
					break;

				if (recTmpData != null) {
					byte[] tmp = new byte[recData.length + recTmpData.length];
					System.arraycopy(recTmpData, 0, tmp, 0, recTmpData.length);
					System.arraycopy(recData, 0, tmp, recTmpData.length,
							recData.length);
					recData = tmp;
				}

				Serializable s = ObjectHandler.FromBytes(recData);

				if (!(s instanceof TransferPackage)) {
					if (recData.length >= 50000000) {
						logPrint("Error: Read package error.");
						recTmpData = null;
					} else {
						recTmpData = recData;
					}
					continue;
				}

				recTmpData = null;

				TransferPackage tp = (TransferPackage) s;
				DataType dbType = tp.getDataType();
				boolean flag = checkSignature(tp);
				if (!flag) {
					// signature error, ignore the package.
					logPrint("signature error.");
					continue;
				}
				if (tp.IsEncrypt()) {
					decryptPackage(tp);
				}
				boolean needReply = true;
				switch (dbType) {
				case command:
					Serializable cmdObj = ObjectHandler.FromBytes(tp.getData());
					if (cmdObj instanceof CommandPackage) {
						CommandPackage cmdpkg = (CommandPackage) cmdObj;
						CommandType type = cmdpkg.getCmdType();
						boolean isError = false;
						// getServerPublicKey don't require login
						if (type != CommandType.getServerPublicKey && 
							type != CommandType.login &&
							type != CommandType.close &&
							type != CommandType.registerSubThread
						) {
							if (!client.isAuthorized) {
								cmdpkg = new CommandPackage(
										CommandType.error,
										logPrint("Error: You need to login before send this command."));
								isError = true;
							}
						}
						if (!isError) {
							switch (cmdpkg.getCmdType()) {
							case getServerPublicKey:
								PublicKey serverPublicKey = KDC
										.getServerPublicKey();
								cmdpkg.setParam(serverPublicKey);
								break;
							case checkClientPublicKey:
								cmdpkg = checkClientPublicKey(cmdpkg);
								break;
							case updateClientPublicKey:
								cmdpkg = updateClientPublicKey(cmdpkg);
								break;
							case close:
								needReply = false;
								logPrint("client close.");
								Isfinish = true;
								break;
							case login:
								cmdpkg = checkLoginInfo(cmdpkg);
								tp.setEntype(EncryptType.ShareKey);
								break;
							case registerSubThread:
								cmdpkg = registersubThread(cmdpkg);
								break;
							case GetSessionKey:
								cmdpkg = generateSessionKey(cmdpkg);
								break;
							case getDirectoryAndFileList:
								cmdpkg = getDirAndFilesList(cmdpkg);
								break;
							case changeUserInfo:
								cmdpkg = changeUserInfo(cmdpkg);
								break;
							case getDiskInfo:
								cmdpkg = getDiskInfo(cmdpkg);
								break;
							case DeleteFile:
								cmdpkg = DeleteFile(cmdpkg);
								break;
                            case keepAlive:
                                cmdpkg = new CommandPackage(CommandType.success);
                                break;
							default:
								cmdpkg = new CommandPackage(
										CommandType.error,
										logPrint("Error: Unknow command package. "));
								break;
							}
						}
						tp.setData(ObjectHandler.ToBytes(cmdpkg));

					} else {
						logPrint("Error: Invalid command package.");
					}
					break;
				case data:
					boolean isError = false;
					if (!client.isAuthorized) {
						DataPackage datapkg = new DataPackage(
								FileHandleType.error,
								"unknown",
								logPrint("Error: You need to login before send this command."));
						tp.setData(ObjectHandler.ToBytes(datapkg));
						isError = true;
					}
					if (!isError) {
						Serializable dataObj = ObjectHandler.FromBytes(tp
								.getData());
						if (dataObj instanceof DataPackage) {
							DataPackage datapkg = (DataPackage) dataObj;
							FileHandleType type = datapkg.getHandleType();
							switch (type) {
							case get:
								datapkg = getServerFile(datapkg);
								break;
							case put:
								DataPackage tmp = SaveFileToServer(datapkg);
								datapkg = (tmp != null) ? tmp : datapkg;
								if (tmp == null) {
									needReply = false;
								}
								break;
							default:
								datapkg = new DataPackage(
										FileHandleType.error,
										"unknown",
										logPrint("Error: Unknow command package. "));
								break;
							}
							tp.setData(ObjectHandler.ToBytes(datapkg));
						}
					}

					break;
				default:
					CommandPackage cmdpkg = new CommandPackage(
							CommandType.error, logPrint("Error: Unknow package. "));
					tp.setData(ObjectHandler.ToBytes(cmdpkg));
					break;
				}
				if (tp.IsEncrypt()) {
					encryptPackage(tp);
				}
				// sign package
				SignPackage(tp);
				if (needReply) {
					writer.write(ObjectHandler.ToBytes(tp));
				}
			}
		} catch (SocketTimeoutException e) {
			Console.println("Error: Timeout for the client. Close the connection.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				Console.println(String.format(
						"MSG: clientip = %s, msg = socket closed.",
						Sock.getInetAddress()));
				ClientManageCenter.removeClient(client);
				Sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void encryptPackage(TransferPackage tp) {
		EncryptType type = tp.getEntype();
		if (type == EncryptType.AES) {
			AES aes = new AES();
			tp.setData(aes.EncryptBytes(client.sessionKey, tp.getData()));
		} else if (type == EncryptType.RSA) {
			RSA rsa = new RSA();
			tp.setData(rsa.Encrypt(client.publickey, tp.getData()));
		} else if (type == EncryptType.ShareKey) {
			AES aes = new AES();
			tp.setData(aes.EncryptBytes(ObjectHandler.getSharedkey(),
					tp.getData()));
		}
	}

	private void decryptPackage(TransferPackage tp) {
		EncryptType type = tp.getEntype();
		if (type == EncryptType.AES) {
			AES aes = new AES();
			tp.setData(aes.DecryptBytes(client.sessionKey, tp.getData()));
		} else if (type == EncryptType.RSA) {
			RSA rsa = new RSA();
			tp.setData(rsa.DecryptBytes(KDC.getServerPrivateKey(), tp.getData()));
		} else if (type == EncryptType.ShareKey) {
			AES aes = new AES();
			tp.setData(aes.DecryptBytes(ObjectHandler.getSharedkey(),
					tp.getData()));
		}
	}

	private boolean checkSignature(TransferPackage tp) {
		boolean flag = false;
		SignType signtype = tp.getSignType();
		if (signtype == SignType.ShareKey) {
			flag = ObjectHandler.checkSignByAES(tp.getData(), tp.getSign());
		} else if (signtype == SignType.PrivateKey) {
			flag = ObjectHandler.checkSign(client.publickey, tp.getData(),
					tp.getSign());
		}
		return flag;
	}

	private void SignPackage(TransferPackage tp) {
		if (tp.getSignType() == SignType.ShareKey) {
			tp.setSignType(SignType.ShareKey);
			tp.setSign(ObjectHandler.signDataByAES(tp.getData()));
		} else if (tp.getSignType() == SignType.PrivateKey) {
			tp.setSignType(SignType.PrivateKey);
			tp.setSign(ObjectHandler.signData(KDC.getServerPrivateKey(),
					tp.getData()));
		}
	}

	private String logPrint(String msg) {
		String name = "missing";
		if(client.user != null){
			name = client.user.getUsername();
		}
		Console.println(String.format("MSG: threadid = %d, clientip = %s, username = %s, msg = %s",
				this.getId(), 
				client.ipaddr.toString(), name, msg));
		return msg;
	}

	private CommandPackage getDiskInfo(CommandPackage cmdpkg) {
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof UserLogin)) {
			return new CommandPackage(CommandType.error, logPrint("Error: Check user info error. Not a valid user object. "));
		}
		UserLogin login = (UserLogin) s;
		User user = client.user;
		if (user == null) {
			return new CommandPackage(CommandType.error, logPrint(String.format("Error: Cannot find user %s. ",
							login.getUsername())));
		}
		login.setDiskUsage(user.getDiskId().getUseSpace());
		login.setDiskCapacity(user.getDiskId().getTotalSpace());
		logPrint(String.format("MSG: User %s get his disk infomation. ",
						login.getUsername()));
		return new CommandPackage(CommandType.success, login);
	}
	
	private CommandPackage DeleteFile(CommandPackage cmdpkg){
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof DirectoryInfo)) {
			return new CommandPackage(CommandType.error, logPrint("Error: Check directory info error. Not a valid DirectoryInfo object. "));
		}
		DirectoryInfo info = (DirectoryInfo) s;
		FileManage.DeleteDirectoryInfo(this.baseDirectory, info);
		client.user = ClientManageCenter.DeleteFiles(client.user, info);
		UserLogin login = new UserLogin(client.user.getUsername());
		login.setDiskUsage(client.user.getDiskId().getUseSpace());
		login.setDiskCapacity(client.user.getDiskId().getTotalSpace());
		logPrint(String.format("MSG: User %s delete file(s). ",
						login.getUsername()));
		return new CommandPackage(CommandType.success, login);
	}

	private CommandPackage changeUserInfo(CommandPackage cmdpkg) {
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof UserLogin)) {
			return new CommandPackage(CommandType.error, logPrint("Error: Check user info error. Not a valid user object. "));
		}
		UserLogin login = (UserLogin) s;
		User user = ClientManageCenter.getUserByUserName(login.getUsername());
		if (user == null) {
			return new CommandPackage(CommandType.error, logPrint(String.format("Error: Cannot find user %s. ",
							login.getUsername())));
		}
		user.setFirstName(login.getFirstName());
		user.setLastName(login.getLastName());
		user.setIcon(login.getIcon());
		String pass = login.getPassword();
		if (!pass.equals("")) {
			user.setPassword(pass);
		}
		ClientManageCenter.UpdateUserInfo(user);
		logPrint(String.format("MSG: User %s changed his infomation. ",
						login.getUsername()));
		return new CommandPackage(CommandType.success);
	}
	
	private CommandPackage registersubThread(CommandPackage cmdpkg){
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof UserLogin)) {
			return new CommandPackage(
					CommandType.error,
					logPrint("Error: Check login info error. Not a valid login object. "));
		}		
		UserLogin login = (UserLogin) s;
		String key = String.format("%s:%s", client.ipaddr.toString(), login.getUsername());
		Client c = ClientManageCenter.getClientByKey(key);
		if(c == null){
			return new CommandPackage(CommandType.error, logPrint("Error: Cannot find register infomation. "));
		}
        String password = c.user.getPassword();
        AES aes = new AES();
        byte[] e = aes.EncryptBytes(c.sessionKey, password.getBytes());
        password = sftp.lib.Utility.printBase64Binary(e);
        if(!login.getPassword().equals(password)){
			return new CommandPackage(CommandType.error, logPrint("Error: register infomation not equal to our records. "));       	
        }
        //register thread count
        synchronized(c){
        	c.threadCount += 1;
        }
        //register baseDirectory
		String userPath = baseDirectory + File.separator
				+ c.user.getDiskId().getDirectory();
		if (!c.user.getDiskId().getDirectory().equals("")
				&& FileManage.checkDir(userPath)) {
			baseDirectory = userPath;
		}
        client = c;
        return new CommandPackage(CommandType.success);
	}

	private CommandPackage checkLoginInfo(CommandPackage cmdpkg) {
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof UserLogin)) {
			return new CommandPackage(
					CommandType.error,
					logPrint("Error: Check login info error. Not a valid login object. "));
		}
		UserLogin login = (UserLogin) s;
		User user = ClientManageCenter.checkUser(login, client);
		if (user == null) {
			return new CommandPackage(CommandType.error, logPrint("Error: Invalid username or password. "));
		}
		client.user = user;
		String publicKey = client.user.getPublickey();
		if (!publicKey.equals("")) {
			byte[] keybytes = Utility.parseBase64Binary(publicKey);
			client.publickey = RSA.getPublicKeyfrombytes(keybytes);
		}
		String userPath = baseDirectory + File.separator
				+ user.getDiskId().getDirectory();
		if (!user.getDiskId().getDirectory().equals("")
				&& FileManage.checkDir(userPath)) {
			baseDirectory = userPath;
		}
		login.setFirstName(user.getFirstName());
		login.setLastName(user.getLastName());
		login.setIcon(user.getIcon());
		login.setDiskUsage(user.getDiskId().getUseSpace());
		login.setDiskCapacity(user.getDiskId().getTotalSpace());
		client.isAuthorized = true;
		try{
			ClientManageCenter.addClient(client);
		}catch(Exception e){
			return new CommandPackage(CommandType.error, logPrint(e.getMessage()));
		}
		logPrint("MSG: login success");
		return new CommandPackage(CommandType.success, login);
	}

	private CommandPackage getDirAndFilesList(CommandPackage cmdpkg) {
		Serializable s = cmdpkg.getParam();
		if (!(s instanceof DirectoryInfo)) {
			return new CommandPackage(CommandType.error, logPrint("Error: Not a valid Directory info object. "));
		}
		DirectoryInfo info = (DirectoryInfo) s;
		try {
			info = FileManage.getDirectoryInfo(this.baseDirectory,
					info.getDirectory());
		} catch (NoSuchFileException e) {
			return new CommandPackage(CommandType.error, logPrint("Error: No such directory. "));
		}

		logPrint(
				String.format("MSG: ask to get directory(%s) list. ",
						info.getDirectory()));
		cmdpkg.setCmdType(CommandType.success);
		cmdpkg.setParam(info);
		return cmdpkg;
	}

	private CommandPackage checkClientPublicKey(CommandPackage cpg) {
		Serializable s = cpg.getParam();
		if (!(s instanceof String)) {
			return new CommandPackage(
					CommandType.error,
					logPrint("Error: Check client public key error. Not a valid publickey object. "));
		}
		String key = (String) s;
		PublicKey clientkey = client.publickey;
		if (clientkey == null) {
			return new CommandPackage(CommandType.error, logPrint("Error: server missing client public key."));
		}
		SHA256 sha = new SHA256();
		String hashValue = sha.EncryptToString(clientkey);
		if (hashValue.equals(key)) {
			logPrint("check public key success.");
			return new CommandPackage(CommandType.success);
		}
		return new CommandPackage(CommandType.error, logPrint("Error: not the same public key."));
	}

	private CommandPackage updateClientPublicKey(CommandPackage cpg) {
		Serializable s = cpg.getParam();
		if (!(s instanceof PublicKey)) {
			return new CommandPackage(
					CommandType.error,
					logPrint("Error: not a valid publickey object. Check client public key error."));
		}
		PublicKey key = (PublicKey) s;
		client.publickey = key;
		client.user.setPublickey(Utility.printBase64Binary(key.getEncoded()));
		ClientManageCenter.UpdateUserInfo(client.user);
		logPrint("update client public key success.");
		return new CommandPackage(CommandType.success);
	}

	private CommandPackage generateSessionKey(CommandPackage cmdpkg) {
		client.sessionKey = KDC.generateAESKey();
		String strkey = Utility.printBase64Binary(client.sessionKey);
		cmdpkg.setParam(strkey);
		logPrint("generate session key success.");
		return cmdpkg;
	}

	private DataPackage getServerFile(DataPackage datapkg) {
		String directory = datapkg.getDirectory();
		if (!directory.equals("/")) {
			if(!datapkg.getDirectory().endsWith("/"))
				directory +=  "/";
		}
		String source = baseDirectory + directory + datapkg.getFileName();
		if (file == null || fis == null || datapkg.getCurrentPackage() == 1) {
			if (!FileManage.checkFile(source)) {
				return new DataPackage(FileHandleType.error, 
						datapkg.getFileName(),
						logPrint("Error: No such file or directory. "));
			}
			try {
				file = new File(source);
				fis = new FileInputStream(source);
				long size = file.length();
				long pkgsize = size / MAXTransfer;
				if (size % MAXTransfer != 0) {
					pkgsize++;
				}
				datapkg.setTotalPackage(pkgsize);
				datapkg.setFilesize(size);
				logPrint(String.format("transfer file %s begin.", source));
			} catch (IOException e) {
				return new DataPackage(FileHandleType.error, 
						datapkg.getFileName(),
						logPrint(e.getMessage()));
			}
		}

		if (datapkg.getCurrentPackage() == preIndex) {
			datapkg.setData(Data);
			return datapkg;
		}

		try {
			int retSize = fis.read(Data, 0, MAXTransfer);
			if (retSize > 0 && retSize != MAXTransfer) {
				byte[] tmp = new byte[retSize];
				System.arraycopy(Data, 0, tmp, 0, retSize);
				Data = tmp;
			}
			if ((retSize != MAXTransfer)
					&& (datapkg.getCurrentPackage() != datapkg
							.getTotalPackage()) || (retSize < 0)) {
				return new DataPackage(FileHandleType.error, 
						datapkg.getFileName(),
						logPrint("Error: cannot read to the end."));
			}
			datapkg.setData(Data);
			preIndex = datapkg.getCurrentPackage();
		} catch (IOException e) {
			return new DataPackage(FileHandleType.error, 
					datapkg.getFileName(),
					logPrint("Error: " + e.getMessage()));
		}

		if (datapkg.getCurrentPackage() == datapkg.getTotalPackage()) {
			// resize data array if it is too small
			if (Data.length < MAXTransfer) {
				Data = new byte[MAXTransfer];
			}
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			file = null;
			fis = null;
			logPrint(String.format("transfer file %s end.", source));
		}

		return datapkg;
	}

	private DataPackage SaveFileToServer(DataPackage datapkg) {
		String directory = datapkg.getDirectory();
		if (!directory.equals("/")) {
			if(!datapkg.getDirectory().endsWith("/"))
				directory +=  "/";
		}
		String source = baseDirectory + directory + datapkg.getFileName();
		if (datapkg.getHandleType() == FileHandleType.error) {
			return null;
		}
		long current = datapkg.getCurrentPackage();
		long total = datapkg.getTotalPackage();
		if (current == 1) {
			FileRecData = null;
			try {
				client.user = ClientManageCenter
						.UpdateUserDisk(client.user, directory,
								datapkg.getFileName(), datapkg.getFilesize());
			} catch (Exception e) {
				logPrint(String.format(
						"File %s was ignored because the disk was full.",
						datapkg.getFileName()));
				return new DataPackage(FileHandleType.error, datapkg.getFileName(),
						"Error: User disk is full");
			}
			logPrint(String.format("receive file %s begin.",
							datapkg.getFileName()));
		}
		if (FileRecData == null) {
			FileRecData = datapkg.getData();
		} else {
			byte[] tmp = new byte[FileRecData.length + datapkg.getData().length];
			System.arraycopy(FileRecData, 0, tmp, 0, FileRecData.length);
			System.arraycopy(datapkg.getData(), 0, tmp, FileRecData.length,
					datapkg.getData().length);
			FileRecData = tmp;
		}
		// save to temp file every 50M
		if (FileRecData.length > 50000000 && (current != total)) {
			try {
				// save to disk
				String tmpfilename = source + ".tmp";
				if (!FileManage.checkFile(tmpfilename)) {
					FileManage.createFile(tmpfilename);
				}
				FileOutputStream fos = new FileOutputStream(tmpfilename, true);
				fos.write(FileRecData);
				fos.close();
				FileRecData = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		datapkg.setData(null);
		datapkg.setCurrentPackage(datapkg.getCurrentPackage() + 1);
		if (current == total) {
			try {
				FileManage.deleteFileIfExists(source);
				String tmpfilename = source + ".tmp";
				if (FileManage.checkFile(tmpfilename)) {
					FileManage.renameFile(tmpfilename, source);
				}
				FileOutputStream fos = new FileOutputStream(source, true);
				fos.write(FileRecData);
				fos.close();

				logPrint(String.format(
						"receive file %s end. Save it to %s",
						datapkg.getFileName(),
						baseDirectory + datapkg.getDirectory()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return new DataPackage(FileHandleType.success,
					datapkg.getFileName(),
					"");
		}
		return datapkg;
	}
}
