/**
 *
 * @file Server.java
 * @author yanwsh
 * @date Mar 22, 2014
 */
package sftp.server;

import java.io.*;
import java.net.*;
import java.util.*;

import sftp.lib.Console;

public class Server extends Thread {
	private ServerSocket servSock;
	private Config conf;
	private Database db;
	
	public Server(Database db, Config conf){
		this.db = db;
		this.conf = new Config(conf);
	}
	
	public boolean isRun(){
		return (servSock != null && !servSock.isClosed());
	}
	
	@Override
	public void start(){
		if(isRun()){
			Console.println("Error: Server already started.");
			return;
		}
		try {
			servSock = new ServerSocket(conf.getPort(), conf.getMaxConnect());
			super.start();
			Console.println("Server is ready.");
		} catch (Exception e) {
			e.printStackTrace();
			servSock = null;
		}
	}
	
	public void close(){
		if(servSock != null){
			try {
				servSock.close();
				Console.println("Server is closed.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void run(){
		if(servSock == null){
			Console.println("Error: ServerSocket is null.");
			return;
		}
		while(!servSock.isClosed()){
			try {
				Socket socket = servSock.accept();
				socket.setSoTimeout(180000);
				String dir = conf.getServdir();
				/*
				InetAddress addr = socket.getInetAddress();
				ClientManageCenter.Client client = ClientManageCenter.getClientList().get(addr.toString());
				if(client != null){
					if(client.user != null){
						dir += File.separator + client.user.getDiskId().getDirectory();
					}
				}*/
				new JobHandler(socket, dir, conf.getMaxPkgSize()).start();
			} 
			catch (SocketException e){
				if(!e.getMessage().equalsIgnoreCase("Socket closed")){
					e.printStackTrace();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
