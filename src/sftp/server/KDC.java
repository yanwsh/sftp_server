package sftp.server;

import java.io.Serializable;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import sftp.lib.crypto.AES;
import sftp.lib.crypto.RSA;

public class KDC {
	private static PublicKey ServerPublicKey;
	private static PrivateKey  ServerPrivateKey;
	private static final int Default_AESLength = 128;
	private static final int Default_RSALength = 1024;
	
	public static byte[] generateAESKey(){
		AES aes = new AES();
		return aes.generateKey(Default_AESLength);
	}
	
	public static KeyPair generateRSAKey(){
		RSA rsa = new RSA();
		return rsa.generateKey(Default_RSALength);
	}

	/**
	 * @return the serverPublicKey
	 */
	public static PublicKey getServerPublicKey() {
		return ServerPublicKey;
	}

	/**
	 * @param serverPublicKey the serverPublicKey to set
	 */
	public static void setServerPublicKey(PublicKey serverPublicKey) {
		ServerPublicKey = serverPublicKey;
	}

	/**
	 * @param serverPrivateKey the serverPrivateKey to set
	 */
	public static void setServerPrivateKey(PrivateKey serverPrivateKey) {
		ServerPrivateKey = serverPrivateKey;
	}

	/**
	 * @return the serverPrivateKey
	 */
	public static PrivateKey getServerPrivateKey() {
		return ServerPrivateKey;
	}
	
}
