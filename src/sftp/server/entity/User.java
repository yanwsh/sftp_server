package sftp.server.entity;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.*;

import java.sql.Time;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Table(name="User")
public class User implements Serializable {  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String username;
	private String password;
	private String publickey;
	private Time LastVisit;
	private String Lastip;
	private String FirstName;
	private String LastName;
	@Column(columnDefinition = "TEXT")
	private String icon;
	
	@OneToOne(cascade = CascadeType.PERSIST, mappedBy="userId")
	private Disk diskId;
	
	private static final long serialVersionUID = 1L;

	public User() {
	}   
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
		this.publickey = "";
		this.LastVisit = new Time(System.currentTimeMillis());
		this.Lastip = "";
		this.FirstName = "";
		this.LastName = "";
		this.icon = "";
	}

	public void clone(User user){
		this.username = user.username;
		this.password = user.password;
		this.publickey = user.publickey;
		this.LastVisit = user.LastVisit;
		this.Lastip = user.Lastip;	
		this.FirstName = user.FirstName;
		this.LastName = user.LastName;
		this.icon = user.icon;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getPublickey() {
		return this.publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}   
	public Time getLastVisit() {
		return this.LastVisit;
	}

	public void setLastVisit(Time LastVisit) {
		this.LastVisit = LastVisit;
	}

	public String getIp() {
		return Lastip;
	}

	public void setIp(String Lastip) {
		this.Lastip = Lastip;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public Disk getDiskId() {
		return diskId;
	}

	public void setDiskId(Disk diskId) {
		this.diskId = diskId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
   
}
