package sftp.server.entity;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: File
 *
 */
@Entity
@Table(name="File")
public class File implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne()
	@JoinColumn(name="Disk_ID")
	private Disk Disk;
	
	private String directory;
	
	private String fileName;
	
	private long size;
	
	private Time CreateTime;
	

	public File() {
	}

	public File(String directory, String fileName, long size, Time createTime) {
		super();
		this.directory = directory;
		this.fileName = fileName;
		this.size = size;
		CreateTime = createTime;
	}



	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the disk
	 */
	public Disk getDisk() {
		return Disk;
	}


	/**
	 * @param disk the disk to set
	 */
	public void setDisk(Disk disk) {
		Disk = disk;
	}


	/**
	 * @return the directory
	 */
	public String getDirectory() {
		return directory;
	}


	/**
	 * @param directory the directory to set
	 */
	public void setDirectory(String directory) {
		this.directory = directory;
	}


	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}


	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}


	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}


	/**
	 * @return the createTime
	 */
	public Time getCreateTime() {
		return CreateTime;
	}


	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Time createTime) {
		CreateTime = createTime;
	}
   
	
}
