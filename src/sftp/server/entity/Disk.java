package sftp.server.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Disk
 *
 */
@Entity
@Table(name="Disk")
public class Disk implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@OneToOne()
	@JoinColumn(name="USER_ID")
	private User userId;
	
	private String directory;
	
	private long totalSpace;
	
	private long useSpace;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="Disk")
	private Collection<File> files = new ArrayList<File>();
	
	public Disk() {
		totalSpace = 0;
		useSpace = 0;
	}
	
	public void clone(Disk disk){
		totalSpace = disk.totalSpace;
		useSpace = disk.useSpace;
		directory = disk.directory;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return userId;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.userId = user;
	}

	/**
	 * @return the totalSpace
	 */
	public long getTotalSpace() {
		return totalSpace;
	}

	/**
	 * @param totalSpace the totalSpace to set
	 */
	public void setTotalSpace(long totalSpace) {
		this.totalSpace = totalSpace;
	}

	/**
	 * @return the useSpace
	 */
	public long getUseSpace() {
		return useSpace;
	}

	/**
	 * @param useSpace the useSpace to set
	 */
	public void setUseSpace(long useSpace) {
		this.useSpace = useSpace;
	}

	/**
	 * @return the directory
	 */
	public String getDirectory() {
		return directory;
	}

	/**
	 * @param directory the directory to set
	 */
	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public Collection<File> getFiles() {
		return files;
	}

	public void setFiles(Collection<File> files) {
		this.files = files;
	}
	
	
}
